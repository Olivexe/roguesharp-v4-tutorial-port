Current:

After only a few days this project has moved to Github: https://github.com/Olivexe/RogueSharpTutorialUnityPort

Too many ghost files in the different branches on Bitbucket and getting annoying. The project will remain for a period of time on Bitbucket but I will not be making any further commits here.



Old:

A port of Faron Bracy's RogueSharp V3 tutorial using the RogueSharp V4 code. https://roguesharp.wordpress.com/. The project advances as far as Step 18, Creating Stairs in Faron's tutorial.
 
This port is fully integrated with Unity! Because of the Unity integration and some of my personal preferences, there are numerous changes to the original tutorial as written by Faron. Some of major changes:
 
1. Uses Unity (version 2018.2.1f1) instead of RLNet Console, so no more RLColor or RLConsole calls;
2. The code has been reorganized mostly in a MVC pattern. There are no dependencies in the Model and Controller logic against Unity. Only View logic has any reliance on Unity. This could make ports on other consoles/backends easier (maybe?);
3. Uses diagonal movement in RogueSharp v4 including monster pathfinding;
4. Most static references removed, passes data by reference. Personal preference, I�m not big on lots of static functions;
5. Uses RogueSharp v4, not v3 as in the original tutorial series. Only caused a few minor changes to the tutorial code;
6. Object pooling used from Catlike coding,https://catlikecoding.com/unity/tutorials/ for Unity GameObjects that represent cells on the console. This was necessary as the height/width sizes of the console cause too many GameObects to be created (thousands). This caused significant slowdowns in the FPS, so I needed an optimization early in the process. As a result, the View logic will only display tiles/cells that are visible in the Camera and dynamically change the viewable tiles/cells as the player moves. This allows for arbitrarily large maps;
7. There are few odds and ends added to the code that are placeholders for a future implementation that allows switching between ASCII characters and graphical tiles.

There are now two branches:
1. Master - Follows the RogueSharp v3 tutorial through section for adding stairs.
2. Adding_To_Tutorial - A complete port of the final RogueSharp tutorial. Also uses RogueSharp v4.1. Except for the Point and WeightedPoint structs. These were changed back to classes.

Enjoy